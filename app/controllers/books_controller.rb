class BooksController < ApplicationController
  def show
    @books = Book.find params[:id]
  end

  def new
  end

  def create
    book = Book.new params.require(:book).permit(:title)
    book.save
    redirect_to book
  end
end
