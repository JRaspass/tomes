FROM ruby:2.7.0-alpine3.11

RUN apk --no-cache add build-base postgresql-dev

COPY Gemfile* ./

RUN bundle install

# Rails isn't Ruby 2.7 ready yet https://stackoverflow.com/questions/59491848
ENV RUBYOPT -W:no-deprecated

CMD ["rails", "s", "-b", "0.0.0.0"]
