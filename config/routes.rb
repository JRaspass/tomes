Rails.application.routes.draw do
  resources :books, only: %w[create new show]
end
